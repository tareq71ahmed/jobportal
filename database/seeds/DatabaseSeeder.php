<?php

use Illuminate\Database\Seeder;
use App\Category;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory('App\User',20)->create();
        factory('App\Company',20)->create();
        factory('App\Job',20)->create();


        //Direct Faker
        $categories=[
            'Government','NGO','Banking','Software Company','Networking','Electrician'
        ];
        foreach($categories as $category){
            Category::create(['name'=>$category]);

        }
        //Direct faker End

    }
}
