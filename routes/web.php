<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

  Route::get('/','JobController@index');

  //jobs Profile
  Route::get('/jobs/{id}/{job}','JobController@show')->name('jobs.show');
  Route::get('/jobs/create','JobController@create')->name('jobs.create');
  Route::post('/jobs/store','JobController@store')->name('jobs.store');
  Route::get('/jobs/myjobs','JobController@myjobs')->name('jobs.myjobs');
  Route::post('/jobs/apply/{id}','JobController@apply')->name('jobs.apply');
  Route::get('/jobs/applicant','JobController@applicant');
  Route::get('/jobs/alljobs','JobController@alljobs')->name('alljobs');


  //Vue
   //apply
  Route::post('/applications/{id}','JobController@apply')->name('apply');
  //save and unsave job vue
  Route::post('/save/{id}','FavoriteController@saveJob');
  Route::post('/unSave/{id}','FavoriteController@unSaveJob');
  //Search Job With vue Js
  Route::get('/jobs/search','JobController@searchJob');

  //Vue End





  //Company Profile
  Route::get('/company/{id}/{company}','CompanyController@index')->name('company.index');
  Route::get('/company/create','CompanyController@create')->name('company.create');
  Route::post('/company/store','CompanyController@store')->name('company.store');
  Route::post('/company/coverphoto','CompanyController@coverphoto')->name('company.coverphoto');
  Route::post('/company/logo','CompanyController@logo')->name('company.logo');
  Route::get('/company','CompanyController@company')->name('company');


  //User rofile
  Route::get('user/profile','UserprofileController@index')->name('user.profile');
  Route::post('profile/store','UserprofileController@store')->name('profile.store');
  Route::post('profile/coverletter','UserprofileController@coverletter')->name('profile.coverletter');
  Route::post('profile/resume','UserprofileController@resume')->name('profile.resume');
  Route::post('profile/avatar','UserprofileController@avatar')->name('profile.avatar');



   //Employee Profile

  Route::view('employer/profile','auth.emp-register')->name('employer.registration');
  Route::post('employer/profile/store','EmployerProfileController@store')->name('employer.store');




  Auth::routes(['verify' => true]);

  Route::get('/home', 'HomeController@index')->name('home');



  // Category
  Route::get('/category/{id}', 'CategoryController@index')->name('category.index');


  //Job refer
 Route::post('job/refer','EmailController@send')->name('mail');
//  /email/job






