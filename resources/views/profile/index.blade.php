
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row ">
            <div class="col-md-2">

                @if(empty(Auth::user()->profile->avatar))
                    <img width="100" style="width: 100%; border-radius:5px;"
                         src="{{asset('avatar/avatar.jpg')}}">
                    @else
                    <img width="100" style="width: 100%; border-radius:5px;"
                         src="{{asset('uploads/avatar')}}/{{Auth::user()->profile->avatar}} ">

                    @endif

                <form action="{{route('profile.avatar')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <br>
                    <div class="card">
                        <div class="card-header">Change Avatar</div>
                        <div class="card-body">
                            <input type="file" name="avatar" class="form-control">
                            <br>
                            <button class="btn btn-sm btn-success">Update</button>
                        </div>
                        @if($errors->has('avatar'))
                            <div class="error" style="color: red">
                                {{$errors->first('avatar')}}
                            </div>
                        @endif

                    </div>
                </form>



            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Update Your Information</div>
                    <div class="card-body">
                        <form action="{{route('profile.store')}}" method="post">
                            @csrf
                        <div class="form-group">
                            <label>Address</label>
                            <textarea name="address" class="form-control" rows="3">{{Auth::user()->profile->address}}</textarea>

                        </div>
                            @if($errors->has('address'))
                                <div class="error" style="color: red">
                                    {{$errors->first('address')}}
                                </div>
                            @endif


                            <div class="form-group">
                                <label>Phone Number</label>
                                <input value="{{Auth::user()->profile->phone_number}}" type="text" name="phone_number" class="form-control" >

                            </div>

                            @if($errors->has('phone_number'))
                                <div class="error" style="color: red">
                                    {{$errors->first('phone_number')}}
                                </div>
                            @endif


                        <div class="form-group">
                            <label>Experience</label>
                            <textarea name="experience" class="form-control" rows="3">{{Auth::user()->profile->experience}}</textarea>

                        </div>

                            @if($errors->has('experience'))
                                <div class="error" style="color: red">
                                    {{$errors->first('experience')}}
                                </div>
                            @endif



                        <div class="form-group">
                            <label>BioData</label>
                            <textarea name="bio" class="form-control" rows="3">{{Auth::user()->profile->bio}}</textarea>

                        </div>

                            @if($errors->has('bio'))
                                <div class="error" style="color: red">
                                    {{$errors->first('bio')}}
                                </div>
                            @endif

                        <div class="form-group">
                        <button class="btn btn-success" type="submit">Submit</button>
                        </div>


                        </form>
                    </div>

                </div>

            </div>
            <div class="col-md-4">

                <div class="card">
                    <div class="card-header">User Details</div>
                    <div class="card-body">
                        <p><b>Name:</b>&nbsp;{{Auth::user()->name}}</p>
                        <p><b>Email:</b>&nbsp;{{Auth::user()->email}}</p>
                        <p><b>Address:</b>&nbsp;{{Auth::user()->profile->address}}</p>
                        <p><b>Phone Number:</b>&nbsp;{{Auth::user()->profile->phone_number}}</p>
                        <p><b>Experience:</b>&nbsp;{{Auth::user()->profile->experience}}</p>
                        <p><b>BioData:</b>&nbsp;{{Auth::user()->profile->bio}}</p>
                        <p><b>Memeber Since:</b>&nbsp;{{date('F d, Y', strtotime(Auth::user()->profile->created_at))}}</p>

                            {{--   Cover Letter--}}
                           @if(!empty(Auth::user()->profile->cover_letter))
                               <p>
                                   <a href="{{Storage::url(Auth::user()->profile->cover_letter)}}">Download Cover Letter</a>
                               </p>
                            @else
                               <p>Please Upload Your Cover Letter..</p>

                           @endif
                        {{--   Cover Letter End--}}

                        {{--   Resume--}}

                        @if(!empty(Auth::user()->profile->resume))
                            <p>
                                <a href="{{Storage::url(Auth::user()->profile->resume)}}">Download Resume</a>
                            </p>
                        @else
                            <p>Please Upload Your Resume..</p>

                        @endif
                        {{--   Resume End --}}




                    </div>
                </div>


                <form action="{{route('profile.coverletter')}}" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="card">
                <div class="card-header">Update Cover Letter</div>
                <div class="card-body">
                    <input type="file" name="cover_letter" class="form-control">
                    <br>
                    <button class="btn  btn-success">Update</button>
                </div>

                        @if($errors->has('cover_letter'))
                            <div class="error" style="color: red">
                                {{$errors->first('cover_letter')}}
                            </div>
                        @endif

                </div>
                </form>



                <form action="{{route('profile.resume')}}" method="post" enctype="multipart/form-data">
                    @csrf
                <div class="card">
                    <div class="card-header">Update Resume</div>
                    <div class="card-body">
                        <input type="file" name="resume" class="form-control">
                        <br>
                        <button class="btn  btn-success">Update</button>
                    </div>

                </div>


                    @if($errors->has('resume'))
                        <div class="error" style="color: red">
                            {{$errors->first('resume')}}
                        </div>
                    @endif


                </form>







            </div>



        </div>
    </div>
@endsection

