  @component('mail::message')

     Hi {{$data['friend_name']}}  {{$data['your_name']}}  Send You A JOb Offer

 @component('mail::button', ['url' => $data['jobUrl']])
 Check
 @endcomponent

 Thanks,<br>
 {{ config('app.name') }}
  @endcomponent
