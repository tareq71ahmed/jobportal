

@extends('layouts.main')

@section('content')
    <div class="site-section bg-light">

        <div class="container">
            <div class="row">
                <div class="company-profile">
                    @if(empty(Auth::user()->company->cover_photo))
                        <img width="100" style="width: 100%; border-radius:5px;"
                             src="{{asset('avatar/avatar.jpg')}}">
                    @else
                        <img  width="1000px" height="300"
                              src="{{asset('uploads/cover')}}/{{Auth::user()->company->cover_photo}} ">

                    @endif

                </div>
                <div class="company-desc"><br>

                    @if(empty(Auth::user()->company->logo))
                        <img width="100" style="width: 100%; border-radius:5px;"
                             src="{{asset('avatar/avatar.jpg')}}">
                    @else
                        <img width="100" style="width: 100%; border-radius:5px;"
                             src="{{asset('uploads/avatar')}}/{{Auth::user()->company->logo}} ">

                    @endif




                    <h1>{{$company->cname}}</h1>
                    <p>{{$company->description}}</p>
                    <p><b>slogan:</b>&nbsp;{{$company->slogan}}</p>
                    <p><b>Address:</b>&nbsp;{{$company->address}}</p>
                    <p><b>Phone:</b>&nbsp;{{$company->phone}}</p>
                    <p><b>website:</b>&nbsp;{{$company->website}}</p>

                </div>

                <table class="table">
                    <thead>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    </thead>

                    <tbody>


                    @foreach($company->jobs as $job)
                        <tr>
                            <td> <img src="{{asset('avatar/avatar.jpg')}}" width="100"> </td>

                            <td>
                                Position: {{$job->position}}
                                <br>
                                Job Type: &nbsp; <i class="fa fa-clock"></i> {{$job->type}}
                            </td>
                            <td>
                                <i class="fa fa-map-marker" style="color: red;"></i> Address : {{$job->address}}
                            </td>
                            <td>
                                <i class="fa fa-calendar" style="color: Blue;"></i> &nbsp; Date: {{$job->created_at->diffForHumans()}}
                            </td>
                            <td>
                                <a href="{{route('jobs.show',[$job->id,$job->slug])}}">
                                    <button class="btn btn-success">Apply</button>
                                </a>
                            </td>
                        </tr>

                    @endforeach



                    </tbody>



                </table>



            </div>
        </div>
    </div>
@endsection




