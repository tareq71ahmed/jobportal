


@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row ">
            <div class="col-md-2">

                @if(empty(Auth::user()->company->logo))
                    <img width="100" style="width: 100%; border-radius:5px;"
                         src="{{asset('avatar/avatar.jpg')}}">
                @else
                    <img width="100" style="width: 100%; border-radius:5px;"
                         src="{{asset('uploads/avatar')}}/{{Auth::user()->company->logo}} ">

                @endif

                <form action="{{route('company.logo')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <br>
                    <div class="card">
                        <div class="card-header">Change Company  Avatar</div>
                        <div class="card-body">
                            <input type="file" name="logo" class="form-control">
                            <br>
                            <button class="btn btn-sm btn-success">Update</button>
                        </div>
                        @if($errors->has('logo'))
                            <div class="error" style="color: red">
                                {{$errors->first('logo')}}
                            </div>
                        @endif

                    </div>
                </form>
            </div>



            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Update Your Company Information</div>
                    <div class="card-body">
                        <form action="{{route('company.store')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Address</label>
                                <textarea name="address" class="form-control" rows="3"></textarea>

                            </div>
                            @if($errors->has('address'))
                                <div class="error" style="color: red">
                                    {{$errors->first('address')}}
                                </div>
                            @endif




                            <div class="form-group">
                                <label>Phone Number</label>
                                <input  type="text" name="phone" class="form-control" >

                            </div>

                            @if($errors->has('phone'))
                                <div class="error" style="color: red">
                                    {{$errors->first('phone')}}
                                </div>
                            @endif




                            <div class="form-group">
                                <label>Website</label>
                                <input  type="text" name="website" class="form-control" >

                            </div>

                            @if($errors->has('website'))
                                <div class="error" style="color: red">
                                    {{$errors->first('website')}}
                                </div>
                            @endif



                            <div class="form-group">
                                <label>Slogan</label>
                                <input  type="text" name="slogan" class="form-control" >

                            </div>

                            @if($errors->has('slogan'))
                                <div class="error" style="color: red">
                                    {{$errors->first('slogan')}}
                                </div>
                            @endif



                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" class="form-control" rows="3"></textarea>

                            </div>

                            @if($errors->has('description'))
                                <div class="error" style="color: red">
                                    {{$errors->first('description')}}
                                </div>
                            @endif




                            <div class="form-group">
                                <button class="btn btn-success" type="submit">Submit</button>
                            </div>


                        </form>


                        @if(Session::has('message'))
                            <div class="alert alert-success">
                                {{Session::get('message')}}
                            </div>
                        @endif

                    </div>

                </div>

            </div>
            <div class="col-md-4">

                <div class="card">
                    <div class="card-header">User Details</div>
                    <div class="card-body">
                        <p><b>Company Name:</b>{{Auth::user()->company->cname}}</p>
                        <p><b>Company Email:</b>{{Auth::user()->email}}</p>
                        <p><b>Company Address:</b>{{Auth::user()->company->address}}</p>
                        <p><b>Company Page:</b>
                           <a href="company/{{Auth::user()->company->slug}}">View</a>
                        </p>
                        <p><b>phone:</b>{{Auth::user()->company->phone}}</p>
                        <p><b>Website:</b>{{Auth::user()->company->website}}</p>
                        <p><b>Slogan:</b>{{Auth::user()->company->slogan}}</p>
                        <p><b>Description:</b>{{Auth::user()->company->description}}</p>
                        <p><b>Updated At:</b>&nbsp;{{date('F d, Y', strtotime(Auth::user()->company->created_at))}}</p>

                    </div>
                </div>


                <form action="{{route('company.coverphoto')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header">Update Cover Photo</div>
                        <div class="card-body">
                            <input type="file" name="cover_photo" class="form-control">
                            <br>
                            <button class="btn  btn-success">Update</button>
                        </div>

                        @if($errors->has('cover_photo'))
                            <div class="error" style="color: red">
                                {{$errors->first('cover_photo')}}
                            </div>
                        @endif

                    </div>
                </form>





            </div>



        </div>
    </div>
@endsection



