@extends('layouts.main')
@section('content')
    <div class="site-section bg-light">
        <div class="container">
            <div class="row">
                @foreach($companies as $company)
                <div class="col-md-3">


                    <div class="card" style="width: 18rem;">


                        @if(empty(Auth::user()->company->cover_photo))
                            <img class="card-img-top" style=" border-radius:5px;"
                                 width="100" height="200"  src="{{asset('avatar/avatar.jpg')}}">
                        @else
                            <img class="card-img-top"  width="100" height="200"
                                  src="{{asset('uploads/cover')}}/{{Auth::user()->company->cover_photo}} ">

                        @endif

                        <div class="card-body">
                            <h5 class="card-title">{{$company->cname}}</h5>

                            <a href="{{route('company.index',[$company->id,$company->slug])}}" class="btn btn-primary">visit Company</a>
                        </div>
                    </div>


                </div>
                @endforeach
            </div>
        </div>

    </div>

@stop
