



@extends('layouts.main')

@section('content')
    <hr>
    <div class="site-section bg-light">
        <div class="container">
            <div class="row ">
                <h1>Recent Jobs</h1><br><br>
                <div class="col-md-12">
                    {{--            search--}}
                    <form action="{{route('alljobs')}}" method="get">


                        <div class="form-inline">

                            <div class="form-group">

                                <input placeholder="Keyword" type="text" name="title" class="form-control">

                            </div>&nbsp; &nbsp;

                            <div class="form-group">

                                <select placeholder="Employment Type" name="type" class="form-control">
                                    <option>Select Category</option>
                                    <option value="fulltime">Full Time</option>
                                    <option value="parttime">Part Time</option>
                                    <option value="casual">Casual</option>

                                </select>

                            </div>&nbsp; &nbsp;

                            <div class="form-group">

                                <select  placeholder="Category" name="category_id" class="form-control">
                                    <option>Select Category</option>
                                    @foreach(App\Category::all() as $cat)
                                        <option value="{{$cat->id}}" >{{$cat->name}}</option>

                                    @endforeach
                                </select>
                            </div>&nbsp; &nbsp;

                            <div class="form-group">

                                <input  placeholder="Address" type="text" name="address" class="form-control">

                            </div>&nbsp;

                            <div class="form-group">

                                <button type="submit" class="btn btn-sm btn-outline-dark">Search..</button>

                            </div>


                        </div>
                    </form>


                    {{--            search--}}

                </div>

                <table class="table">
                    <thead>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    </thead>

                    <tbody>


                    @foreach($jobs as $job)
                        <tr>
                            <td> <img src="{{asset('avatar/avatar.jpg')}}" width="100"> </td>

                            <td>
                                Position: {{$job->position}}
                                <br>
                                Job Type: &nbsp; <i class="fa fa-clock"></i> {{$job->type}}
                            </td>
                            <td>
                                <i class="fa fa-map-marker" style="color: red;"></i> Address : {{$job->address}}
                            </td>
                            <td>
                                <i class="fa fa-calendar" style="color: Blue;"></i> &nbsp; Date: {{$job->created_at->diffForHumans()}}
                            </td>
                            <td>
                                <a href="{{route('jobs.show',[$job->id,$job->slug])}}">
                                    <button class="btn btn-success">Apply</button>
                                </a>
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
                {{$jobs->links()}}



            </div>





        </div>

    </div>
@endsection



