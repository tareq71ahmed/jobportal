@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">


                        <table class="table">
                            <thead>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            </thead>

                            <tbody>


                            @foreach($jobs as $job)
                                <tr>
                                    <td> <img src="{{asset('avatar/avatar.jpg')}}" width="100"> </td>

                                    <td>
                                        Position: {{$job->position}}
                                        <br>
                                        Job Type: &nbsp; <i class="fa fa-clock"></i> {{$job->type}}
                                    </td>
                                    <td>
                                        <i class="fa fa-map-marker" style="color: red;"></i> Address : {{$job->address}}
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar" style="color: Blue;"></i> &nbsp; Date: {{$job->created_at->diffForHumans()}}
                                    </td>
                                    <td>
                                        <a href="{{route('jobs.show',[$job->id,$job->slug])}}">
                                            <button class="btn btn-success">Apply</button>
                                        </a>
                                    </td>
                                </tr>

                            @endforeach



                            </tbody>



                        </table>






                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
