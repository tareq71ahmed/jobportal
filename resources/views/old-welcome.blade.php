


@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <search-component></search-component>

            </div>





            <h1>Recent Jobs</h1>

            <table class="table">
                <thead>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                </thead>

                <tbody>


                @foreach($jobs as $job)
                <tr>
                    <td> <img src="{{asset('avatar/avatar.jpg')}}" width="100"> </td>

                    <td>
                        Position: {{$job->position}}
                        <br>
                        Job Type: &nbsp; <i class="fa fa-clock"></i> {{$job->type}}
                    </td>
                    <td>
                        <i class="fa fa-map-marker" style="color: red;"></i> Address : {{$job->address}}
                    </td>
                    <td>
                        <i class="fa fa-calendar" style="color: Blue;"></i> &nbsp; Date: {{$job->created_at->diffForHumans()}}
                    </td>
                    <td>
                        <a href="{{route('jobs.show',[$job->id,$job->slug])}}">
                        <button class="btn btn-success">Apply</button>
                        </a>
                    </td>
                </tr>

                    @endforeach



                </tbody>



            </table>



        </div>

        <div>

            <a href="{{route('alljobs')}}">
            <button style="width: 100%" class="btn btn-success btn-lg">Browse All Job</button>
            </a>



        </div><br><br>
        <h1>Feature Company</h1>

        <div class="container">
            <div class="row">
                @foreach($companies as $company)
                <div class="col-md-3">

                    <div class="card" style="width: 18rem;">

                        <div style="margin: 1px" class="card-body">
                            <h5 class="card-title">{{$company->cname}}</h5>
                            <p class="card-text">{{str_limit($company->description,20)}}</p>
                            <a href="{{route('company.index',[$company->id,$company->slug])}}" class="btn btn-primary">Visit Company</a>
                        </div>
                    </div>


                </div>
                    @endforeach

            </div>

        </div>



    </div>
@endsection
