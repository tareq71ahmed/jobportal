<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model

{

    protected $guarded=[];

    //Slug
    public  function getRouteKeyName()
    {
        return 'slug';
    }

    //one to many
    public  function jobs(){
        return $this->hasMany('App\Job');
    }





    public function user()
    {
        return $this->belongsTo('App\User');
    }




}
