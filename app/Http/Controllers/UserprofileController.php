<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Middleware\Seeker;


class UserprofileController extends Controller
{

    public function __construct()
    {
      return $this->middleware(['Seeker','verified']);
    }


    public  function index(){
        return view('profile.index');
    }

     public function store(Request $request){
        //validation
         $this->validate($request,[
             'address'=>'required',
             //'phone_number'=>'required|numeric|min:20',
             'phone_number'=>'required|regex:/(01)[0-9]{9}/',
             'experience'=>'required|min:20',
             'bio'=>'required|min:20',


         ]);
         //validation End
         $profile= Profile::where('user_id',Auth::id())->first();
         $profile->address = $request['address'];
         $profile->phone_number = $request['phone_number'];
         $profile->experience = $request['experience'];
         $profile->bio = $request['bio'];
         $profile->save();

         return redirect()->back()->with('message','Your Profile  Updated Successfully');


     }




     public function coverletter(Request $request){
         //validation
         $this->validate($request,[
             'cover_letter'=>'required|mimes:pdf,doc,docs|max:20000',
         ]);
         //validation End

    $profile= Profile::where('user_id',Auth::id())->first();
    $user_id = auth()->user()->id;
    $cover= $request->file('cover_letter')->store('public/files');
    $profile::where('user_id', $user_id)->update(['cover_letter'=>$cover]);
    return redirect()->back()->with('message','Your Profile  Updated Successfully');

       }




    public function resume(Request $request){
        //validation
        $this->validate($request,[
            'resume'=>'required|mimes:pdf,doc,docs|max:20000',
        ]);
        //validation End

        $profile= Profile::where('user_id',Auth::id())->first();
        $user_id = auth()->user()->id;
        $resume= $request->file('resume')->store('public/files');
        $profile::where('user_id', $user_id)->update(['resume'=>$resume]);
        return redirect()->back()->with('message','Your Resume  Updated Successfully');
    }






    public function avatar(Request $request){

        //validation
        $this->validate($request,[
            'avatar'=>'required|mimes:jpg,png,jpeg|max:1024',
        ]);
        //validation End


        $user_id = auth()->user()->id;
            if($request->hasFile('avatar')){
                $profile= Profile::where('user_id',Auth::id())->first();
                $file=$request->file('avatar');
                $text=$file->getClientOriginalExtension();
                $fileName =time().'.'.$text;
                $file->move('uploads/avatar',$fileName);
                $profile::where('user_id', $user_id)->update(['avatar'=>$fileName]);
            }

        return redirect()->back()->with('message','avatar  Updated Successfully');
    }






}
