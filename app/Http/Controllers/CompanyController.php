<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Middleware\Employer;

class CompanyController extends Controller
{

    public function __construct()
    {
        return $this->middleware(['Employer','verified'],['except'=>['company']]);
    }



    public function index($id,Company $company){

        return view('company.index' ,compact('company'));
    }

    public function create(){


        return view('company.create');

    }




    public function store(Request $request){

        //validation
        $this->validate($request,[
            'address'=>'required',
            //'phone_number'=>'required|numeric|min:20',
            'phone'=>'required|regex:/(01)[0-9]{9}/',
            'website'=>'required',
            'slogan'=>'required',
            'description'=>'required',
        ]);
        //validation End

        $company= Company::where('user_id',Auth::id())->first();
        $company->address = $request['address'];
        $company->phone = $request['phone'];
        $company->website = $request['website'];
        $company->slogan = $request['slogan'];
        $company->description = $request['description'];

        $company->save();
        return redirect()->back()->with('message','Your Company Profile  Updated Successfully');


    }





    public function coverphoto(Request $request){
        //validation
        $this->validate($request,[
            'cover_photo'=>'required|mimes:jpg,png,jpeg|max:1024',
        ]);
        //validation End


        $user_id = auth()->user()->id;
        if($request->hasFile('cover_photo')){
            $company= Company::where('user_id',Auth::id())->first();
            $file=$request->file('cover_photo');
            $text=$file->getClientOriginalExtension();
            $fileName =time().'.'.$text;
            $file->move('uploads/cover',$fileName);
            $company::where('user_id', $user_id)->update(['cover_photo'=>$fileName]);
        }

        return redirect()->back()->with('message','CoverPhoto  Updated Successfully');

    }






    public function logo(Request $request){

        //validation
        $this->validate($request,[
            'logo'=>'required|mimes:jpg,png,jpeg|max:1024',
        ]);
        //validation End


        $user_id = auth()->user()->id;
        if($request->hasFile('logo')){
            $company= Company::where('user_id',Auth::id())->first();
            $file=$request->file('logo');
            $text=$file->getClientOriginalExtension();
            $fileName =time().'.'.$text;
            $file->move('uploads/avatar',$fileName);
            $company::where('user_id', $user_id)->update(['logo'=>$fileName]);
        }

        return redirect()->back()->with('message','avatar  Updated Successfully');
    }



     public function company()
     {
         $companies=Company::paginate(20);
         return view('company.company',compact('companies'));
     }


}
